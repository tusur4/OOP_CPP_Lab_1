#include "my_string.h"

errno_t err;

void terminate(const char* message)
{
	printf("%s\n", message);
	exit(EXIT_FAILURE);
}

void my_strcat(char* dest, rsize_t destsz, char* src)
{
	err = strcat_s(dest, destsz, src);

	if (err)
		terminate("ERROR in my_strcat");
}

void my_strcpy(char* dest, rsize_t destsz, char* src)
{
	err = strcpy_s(dest, destsz, src);

	if (err)
		terminate("ERROR in my_strcpy");
}