#include "Address.h"
#include<iostream>

#define LENGTH_STRING 100

const char void_str[] = { "" };

Address::Address()
{
	this->country = new char[LENGTH_STRING];
	this->country[0] = '\0';
	this->city = new char[LENGTH_STRING];
	this->city[0] = '\0';
	this->street = new char[LENGTH_STRING];
	this->street[0] = '\0';
	this->name = new char[3 * LENGTH_STRING];
	this->name[0] = '\0';

	char init_country[] = { "Russian Federation" };

	my_strcpy(this->country, LENGTH_STRING, init_country);
	house_number = 0;

	set_address();

}

Address::Address(char* country, char* city, char* street, int house_number) : Address()
{
	my_strcpy(this->country, LENGTH_STRING, country);
	my_strcpy(this->city, LENGTH_STRING, city);
	my_strcpy(this->street, LENGTH_STRING, street);

	this->house_number = house_number;

	set_address();
}

Address::~Address()
{
	cout << "\n Address. Destructor executed";
}

void Address::set_address(void)
{
	int i;
	char* str_house_number{};
	char buffer[10];
	char space[] = { ", " };
	char dot[] = { "." };
	name[0] = '\0';


	my_strcat(this->name, 3 * LENGTH_STRING, country);
	my_strcat(this->name, 3 * LENGTH_STRING, space);

	my_strcat(this->name, 3 * LENGTH_STRING, city);
	my_strcat(this->name, 3 * LENGTH_STRING, space);

	my_strcat(this->name, 3 * LENGTH_STRING, street);
	my_strcat(this->name, 3 * LENGTH_STRING, space);


	i = sprintf_s(buffer, "%d", house_number);
	if (i <= 0)
		terminate("ERROR in sprintf_s");

	my_strcat(name, 3 * LENGTH_STRING, buffer);
	my_strcat(name, 3 * LENGTH_STRING, dot);
}

void Address::set_country(char* country)
{
	my_strcpy(this->country, LENGTH_STRING, country);
	Address::set_address();
}

void Address::set_city(char* city)
{
	my_strcpy(this->city, LENGTH_STRING, city);
	Address::set_address();
}

void Address::set_street(char* street)
{
	my_strcpy(this->street, LENGTH_STRING, street);
	Address::set_address();
}

void Address::set_house_number(int house_number)
{
	this->house_number = house_number;
	Address::set_address();
}

char* Address::get_country(void)
{
	return this->country;
}

char* Address::get_city(void)
{
	return this->city;
}

char* Address::get_street(void)
{
	return this->street;
}

int Address::get_house_number(void)
{
	return this->house_number;
}

char* Address::get_address(void)
{
	return this->name;
}