#pragma once 
#ifndef MAIN_H	// for legacy compilers
#define MAIN_H
#include "Address.h"
using namespace std;

class Person
{
private:
	int person_number;
	char* full_name;
	bool gender;
	double age;
	Address address;
public:
	Person();
	Person(int person_number, char* full_name, bool gender, double age);
	Person(int person_number, char* full_name, bool gender, double age, char* country, char* city, char* street, int house_number);
	~Person();
	void set_person_number(int person_number);
	void set_full_name(char* full_name);
	void set_gender(bool gender);
	void set_age(double age);
	void set_country(char* country);
	void set_city(char* city);
	void set_street(char* street);
	void set_house_number(int house_number);
	int	get_person_number(void);
	char* get_full_name(void);
	bool get_gender(void);
	double get_age(void);
	char* get_address(void);
	void input(void);
	void print(void);
};

#endif