#pragma once
#ifndef ADDRESS_H	// for legacy compilers
#define ADDRESS_H

#include "my_string.h"
using namespace std;

class Address
{
private:
	char* name;
	char* country;
	char* city;
	char* street;
	int house_number;
	void set_address(void);
public:
	Address();
	Address(char* country, char* city, char* street, int house_number);
	~Address();
	void set_country(char* name);
	void set_city(char* country);
	void set_street(char* city);
	void set_house_number(int house_number);
	char* get_country(void);
	char* get_city(void);
	char* get_street(void);
	int get_house_number(void);
	char* get_address(void);
};

#endif