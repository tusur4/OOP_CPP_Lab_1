#include <iostream>
#include "Main.h"

#define LENGTH_STRING 100

using namespace std;
char void_string[] = "";

void my_getline(char* dest)
{
	// skip empty lines
	char* line = new char[LENGTH_STRING];

	cin.getline(dest, LENGTH_STRING);
	if (dest[0] == '\0')
		cin.getline(dest, LENGTH_STRING);

}

Person::Person(void) : address()
{
	full_name = new char[LENGTH_STRING];
	full_name[0] = '\0';
	person_number = 0;
	my_strcpy(full_name, LENGTH_STRING, void_string);
	gender = false;
	age = 0;

}

Person::Person(int person_number, char* full_name, bool gender, double age) : address()
{

	this->person_number = person_number;
	my_strcpy(this->full_name, LENGTH_STRING, full_name);
	this->gender = gender;
	this->age = age;
}

Person::Person(int person_number, char* full_name, bool gender, double age, char* country, char* city, char* street, int house_number) : address(country, city, street, house_number)
{
	this->person_number = person_number;
	this->full_name = new char[LENGTH_STRING];
	this->full_name[0] = '\0';
	my_strcpy(this->full_name, LENGTH_STRING, full_name);
	this->gender = gender;
	this->age = age;
}

Person::~Person()
{
	cout << "\n Person. Destructor executed";
}

void Person::set_person_number(int person_number)
{
	this->person_number = person_number;
}

void Person::set_full_name(char* full_name)
{
	my_strcpy(this->full_name, LENGTH_STRING, full_name);
}

void Person::set_gender(bool gender)
{
	this->gender = gender;
}

void Person::set_age(double age)
{
	this->age = age;
}

void Person::set_country(char* country)
{
	address.set_country(country);
}

void Person::set_city(char* city)
{
	address.set_city(city);
}

void Person::set_street(char* street)
{
	address.set_street(street);
}

void Person::set_house_number(int house_number)
{
	address.set_house_number(house_number);
}

int	 Person::get_person_number(void)
{
	return this->person_number;
}

char* Person::get_full_name(void)
{
	return this->full_name;
}

bool Person::get_gender(void)
{
	return this->gender;
}

double Person::get_age(void)
{
	return this->age;
}

char* Person::get_address(void)
{
	return address.get_address();
}

void Person::input()
{
	int person_number;
	char full_name[LENGTH_STRING];
	bool gender;
	double age;

	char country[LENGTH_STRING];
	char city[LENGTH_STRING];
	char street[LENGTH_STRING];
	int house_number;

	cout << "\nEnter person number:";
	cin >> person_number;
	cout << "Enter full name:";
	my_getline(&full_name[0]);
	cout << "Enter genter 1- man, 0 - woman:";
	cin >> gender;
	cout << "Enter an age:";
	cin >> age;
	cout << "Enter a country:";
	my_getline(&country[0]);
	cout << "Enter a city:";
	my_getline(&city[0]);
	cout << "Enter a street:";
	my_getline(&street[0]);
	cout << "Enter a house number:";
	for (;;)
	{
		cin >> house_number;
		if (house_number > 0)
			break;
		else
			cout << "The house number can not be negative or zero. Enter a house number:";
	}

	cout << endl;

	this->person_number = person_number;
	my_strcpy(this->full_name, LENGTH_STRING, full_name);
	this->gender = gender;
	this->age = age;
	this->set_country(&country[0]);
	this->set_city(&city[0]);
	this->set_street(&street[0]);
	this->set_house_number(house_number);


}

void Person::print()
{
	cout << get_person_number() << endl;
	cout << get_full_name() << endl;
	if (get_gender())
		cout << "man" << endl;
	else
		cout << "woman" << endl;
	cout << get_age() << endl;
	cout << get_address() << endl;

}

int main()
{
	Person p = Person();

	char full_name_0[] = { "Ivan" };
	char country_0[] = { "Russia" };
	char city_0[] = { "Moskow" };
	char street_0[] = { "Lenina" };

	p.print();
	cout << '\n';

	p.set_country(&country_0[0]);
	p.set_city(&city_0[0]);
	p.set_street(&street_0[0]);
	p.set_age(23.0);
	p.set_full_name(&full_name_0[0]);
	p.set_gender(true);

	p.print();
	cout << '\n';

	char full_name_1[] = { "Ivan Petrovich Ivanov" };
	char country_1[] = { "Russian Federation" };
	char city_1[] = { "Saint Petersburg" };
	char street_1[] = { "Sadovaya" };

	Person p1 = Person(25, &full_name_1[0], true, 23.0, &country_1[0], &city_1[0], &street_1[0], 25);
	p1.print();
	cout << '\n';

	char full_name_2[] = { "Piter Petrovich Sidorov" };
	char country_2[] = { "Russian Federation" };
	char city_2[] = { "Moskow" };
	char street_2[] = { "Sadovaya" };

	Person* p2 = new Person(75, &full_name_2[0], true, 23.0, &country_2[0], &city_2[0], &street_2[0], 25);
	p2->print();
	cout << '\n';

	delete p2;

	Person p3 = Person();
	p3.input();
	p3.print();

	return 0;
}

