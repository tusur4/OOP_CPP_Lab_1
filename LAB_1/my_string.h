#pragma once
#ifndef MY_STRING_H	// for legacy compilers
#define MY_STRING_H

#include <corecrt.h>
#include  <cstdlib>
#include <cstring>
#include <stdio.h>

using namespace std;

void terminate(const char* message);

void my_strcat(char* dest, rsize_t destsz, char* src);

void my_strcpy(char* dest, rsize_t destsz, char* src);

#endif
